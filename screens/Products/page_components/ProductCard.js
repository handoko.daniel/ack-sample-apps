import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Dimensions, Image } from 'react-native';
import Constant from '../../../utilities/Constant';

export default class ProductCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <TouchableOpacity style={localStyles.container}>
                <Image
                    source={{ uri: this.props.data.url }}
                    style={localStyles.image}
                />
                <Text style={localStyles.title}>{this.props.data.name}</Text>
                <Text style={localStyles.thinLabel}>{this.props.data.category}</Text>
                <Text style={localStyles.label}>Rp {this.props.data.price.toString().replace(/(.)(?=(\d{3})+$)/g, '$1.')}</Text>
            </TouchableOpacity>
        );
    }
}

const localStyles = StyleSheet.create({
    image: {
        width: (Dimensions.get('window').width / 2) - 48,
        height: (Dimensions.get('window').width / 2) - 48,
        borderRadius: 8,
        marginBottom: 16
    },
    container: {
        backgroundColor: Constant.COLOR.ABSOLUTE_WHITE,
        justifyContent: 'flex-end',
        borderRadius: 8,
        padding: 16,
        justifyContent: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    title: {
        fontSize: 16,
        color: Constant.COLOR.DARK_LABEL,
        fontWeight: 'bold',
    },
    label: {
        fontSize: 16,
        color: Constant.COLOR.DARK_LABEL,
    },
    thinLabel: {
        fontSize: 12,
        color: Constant.COLOR.THIN_GRAY,
        marginBottom: 8
    },
})