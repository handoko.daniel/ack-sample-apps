import React, { Component } from 'react';
import { View, Text, SafeAreaView, StatusBar, StyleSheet, Dimensions, TextInput, FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import StaticHeader from '../../components/header/StaticHeader';
import Constant from '../../utilities/Constant';
import ProductData from '../Dashboard/data/products.json';
import StoreData from '../Dashboard/data/store.json';
import Store from '../Dashboard/Store';
import StoreCard from '../Store/page_components/StoreCard';
import SearchCard from './page_components/SearchCard';

export default class UniversalSearch extends Component {
    constructor(props) {
        super(props);
        const navigationParams = this.props.route.params.type;
        this.state = {
            data: navigationParams === "product" ? ProductData.data : StoreData.data,
            query: '',
            type: navigationParams
        };
    }

    handleRedirectUser(page, data) {
        if (page == "Back") {
            this.props.navigation.goBack();
        }
        else {
            this.props.navigation.navigate(page, { data: data })
        }
    }

    handleSearchProduct(query) {
        const { type } = this.state;
        let searchResult = []
        if (type == "product") {
            let data = ProductData.data;
            for (let index = 0; index < data.length; index++) {
                if (data[index].name.includes(query)) {
                    searchResult.push(data[index]);
                }
            }
        }
        else {
            let data = StoreData.data;
            for (let index = 0; index < data.length; index++) {
                if (data[index].name.includes(query)) {
                    searchResult.push(data[index]);
                }
            }
        }
        this.setState({ query, data: searchResult })
    }

    renderSearchResult() {
        const { data, type } = this.state;
        if (type == "product") {
            return (
                <FlatList
                    data={data}
                    keyExtractor={item => item.id}
                    contentContainerStyle={{ alignItems: 'center', padding: 8, paddingHorizontal: 16 }}
                    renderItem={({ item, index, separators }) =>
                    (
                        <SearchCard
                            key={index}
                            data={item}
                        />
                    )}
                />
            )
        }
        else {
            return (
                <FlatList
                    data={data}
                    keyExtractor={item => item.id}
                    contentContainerStyle={{ alignItems: 'center', padding: 8, paddingHorizontal: 16 }}
                    renderItem={({ item, index, separators }) =>
                    (
                        <StoreCard
                            key={index}
                            data={item}
                            onPress={() => this.handleRedirectUser('StoreDetail', item)}
                        />
                    )}
                />
            )
        }
    }

    render() {
        return (
            <SafeAreaView style={localStyles.container}>
                <StatusBar
                    barStyle="light-content"
                    backgroundColor={Constant.COLOR.PRIMARY}
                />
                <StaticHeader
                    type="back"
                    onGoBack={() => this.handleRedirectUser('Back')}
                />
                <View style={localStyles.inputContainer}>
                    <Icon
                        name="ios-search"
                        size={24}
                        color={Constant.COLOR.THIN_GRAY}
                    />
                    <TextInput
                        value={this.state.query}
                        placeholder="Search.."
                        style={{ paddingHorizontal: 8 }}
                        onChangeText={(value) => this.handleSearchProduct(value)}
                    />
                </View>
                <View style={localStyles.contentContainer}>
                    {this.renderSearchResult()}
                </View>
            </SafeAreaView>
        );
    }
}

const localStyles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        flex: 1,
        backgroundColor: Constant.COLOR.ABSOLUTE_WHITE,
    },
    contentContainer: {
        backgroundColor: Constant.COLOR.ABSOLUTE_WHITE,
        width: Dimensions.get('window').width,
        flex: 1,
    },
    gridView: {
        marginTop: 12,
        flex: 1,
    },
    image: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').width,
        resizeMode: "cover"
    },
    title: {
        fontSize: 24,
        marginBottom: 8
    },
    label: {
        fontSize: 14,
        color: Constant.COLOR.DARK_LABEL,
        marginBottom: 8,
        lineHeight: 24
    },
    thinLabel: {
        fontSize: 12,
        color: Constant.COLOR.THIN_GRAY,
        marginBottom: 8
    },
    address: {
        fontSize: 12,
        color: Constant.COLOR.DARK_LABEL,
        marginBottom: 4
    },
    inputContainer: {
        flexDirection: 'row',
        padding: 16,
        borderBottomWidth: 0.5,
        borderColor: Constant.COLOR.THIN_GRAY
    }
})
