import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, Image } from 'react-native';
import Icon from 'react-native-vector-icons/Entypo';

// import custom component
import Constant from '../../../utilities/Constant';
import StoreData from '../../Dashboard/data/store.json'

export default class SearchCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    renderStore() {
        const { data } = this.props;
        let store = StoreData.data;
        let storeInformation = "-";

        for (let index = 0; index < store.length; index++) {
            if (store[index].id == data.storeId) {
                storeInformation = store[index].name;
            }
        }

        return (storeInformation);
    }

    render() {
        return (
            <View style={localStyles.container}>
                <Image
                    source={{ uri: this.props.data.url }}
                    style={localStyles.image}
                />
                <View style={{ flex: 1, paddingHorizontal: 16 }}>
                    <Text style={localStyles.title}>{this.props.data.name}</Text>
                    <Text style={localStyles.label}>Rp {this.props.data.price.toString().replace(/(.)(?=(\d{3})+$)/g, '$1.')}</Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center', borderWidth: 0 }}>
                        <Icon
                            name="shop"
                            size={14}
                            color={Constant.COLOR.DARK_LABEL}
                        />
                        <Text style={[localStyles.label, { paddingLeft: 8 }]}>{this.renderStore()}</Text>
                    </View>
                </View>
            </View>
        );
    }
}

const localStyles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width - 32,
        backgroundColor: Constant.COLOR.ABSOLUTE_WHITE,
        padding: 16,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        marginBottom: 16,
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        flexDirection: 'row',
        borderRadius: 8,
    },
    image: {
        width: Dimensions.get('window').width * 0.24,
        height: Dimensions.get('window').width * 0.24,
    },
    title: {
        fontWeight: 'bold',
        fontSize: 16,
        color: Constant.COLOR.DARK_LABEL,
        marginBottom: 4,
        lineHeight: 24
    },
    label: {
        fontSize: 14,
        color: Constant.COLOR.DARK_LABEL,
        lineHeight: 24
    },
})