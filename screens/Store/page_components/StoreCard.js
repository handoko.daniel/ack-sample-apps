import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, Image, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

// import custom component
import Constant from '../../../utilities/Constant';

export default class StoreCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <TouchableOpacity
                style={localStyles.container}
                onPress={this.props.onPress}
            >
                <Image
                    source={{ uri: this.props.data.url }}
                    style={localStyles.image}
                />
                <View style={{ flex: 1, paddingLeft: 16, paddingRight: 0, borderWidth: 0 }}>
                    <Text
                        numberOfLines={2}
                        style={localStyles.title}
                    >
                        {this.props.data.name}
                    </Text>
                    <View style={{ flexDirection: 'row' }}>
                        <Icon
                            name="ios-pin"

                        />
                        <Text style={localStyles.address}>{this.props.data.address}</Text>
                    </View>
                    <Text style={localStyles.thinLabel}>{this.props.data.category}</Text>
                    <Text
                        style={localStyles.label}
                        numberOfLines={3}
                    >{this.props.data.description}</Text>
                </View>
            </TouchableOpacity>
        );
    }
}

const localStyles = StyleSheet.create({
    container: {
        width: "100%",
        backgroundColor: Constant.COLOR.ABSOLUTE_WHITE,
        padding: 16,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        marginBottom: 16,
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        flexDirection: 'row',
        borderRadius: 8,
    },
    image: {
        width: Dimensions.get('window').width * 0.36,
        height: Dimensions.get('window').width * 0.36,
    },
    title: {
        fontWeight: 'bold',
        fontSize: 16,
        color: Constant.COLOR.DARK_LABEL,
        marginBottom: 4,
        lineHeight: 24
    },
    thinLabel: {
        fontSize: 12,
        color: Constant.COLOR.THIN_GRAY,
        marginBottom: 8
    },
    label: {
        fontSize: 14,
        color: Constant.COLOR.DARK_LABEL,
        marginBottom: 8,
        lineHeight: 24
    },
    address: {
        fontSize: 12,
        color: Constant.COLOR.DARK_LABEL,
        marginBottom: 4
    }
})