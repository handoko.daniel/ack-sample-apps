import React, { Component } from 'react';
import { View, Text, SafeAreaView, StatusBar, Dimensions, StyleSheet, Image, ScrollView, FlatList } from 'react-native';
import { FlatGrid } from 'react-native-super-grid';

// import custom component
import StaticHeader from '../../components/header/StaticHeader';
import Constant from '../../utilities/Constant';
import ProductCard from '../Products/page_components/ProductCard';
import ProductData from '../../screens/Dashboard/data/products.json';

export default class StoreDetail extends Component {
    constructor(props) {
        super(props);
        const navigationParams = this.props.route.params.data;
        this.state = {
            data: ProductData.data,
            store: navigationParams
        };
    }

    handleRedirectUser(page) {
        if (page == "Back") {
            this.props.navigation.goBack();
        }
    }

    renderProduct() {
        const { data, store } = this.state;
        let mutateData = [];
        for (let index = 0; index < data.length; index++) {
            if (data[index].storeId == store.id) {
                mutateData.push(data[index])
            }
        }

        return (
            <FlatGrid
                itemDimension={130}
                data={mutateData}
                style={localStyles.gridView}
                showsVerticalScrollIndicator={false}
                spacing={10}
                renderItem={({ item }) => (
                    <ProductCard
                        data={item}
                    />
                )}
            />
        )
    }

    render() {
        return (
            <SafeAreaView style={localStyles.container}>
                <StatusBar
                    barStyle="light-content"
                    backgroundColor={Constant.COLOR.PRIMARY}
                />
                <StaticHeader
                    type="back"
                    onGoBack={() => this.handleRedirectUser('Back')}
                />
                <FlatList
                    ListHeaderComponent={
                        <>
                            <Image
                                source={{ uri: this.state.store.url }}
                                style={localStyles.image}
                            />
                            <View style={localStyles.contentContainer}>
                                <Text style={localStyles.title}>{this.state.store.name}</Text>
                                <Text style={localStyles.address}>{this.state.store.address}</Text>
                                <Text style={localStyles.thinLabel}>{this.state.store.category}</Text>
                                <Text style={localStyles.label}>{this.state.store.description}</Text>
                                <Text style={localStyles.title}>Our Product</Text>
                            </View>
                            <View style={{
                                backgroundColor: Constant.COLOR.ABSOLUTE_WHITE,
                                width: Dimensions.get('window').width,
                                flex: 1,
                            }}>
                                {this.renderProduct()}
                            </View>
                        </>
                    }
                />
            </SafeAreaView>
        );
    }
}

const localStyles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        flex: 1,
        backgroundColor: Constant.COLOR.ABSOLUTE_WHITE,
    },
    contentContainer: {
        padding: 16
    },
    gridView: {
        marginTop: 12,
        flex: 1,
    },
    image: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').width,
        resizeMode: "cover"
    },
    title: {
        fontSize: 24,
        marginBottom: 8
    },
    label: {
        fontSize: 14,
        color: Constant.COLOR.DARK_LABEL,
        marginBottom: 8,
        lineHeight: 24
    },
    thinLabel: {
        fontSize: 12,
        color: Constant.COLOR.THIN_GRAY,
        marginBottom: 8
    },
    address: {
        fontSize: 12,
        color: Constant.COLOR.DARK_LABEL,
        marginBottom: 4
    }
})
