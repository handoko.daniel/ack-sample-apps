import React, { Component } from 'react';
import { View, Text, SafeAreaView, StatusBar, Dimensions, StyleSheet, Image, FlatList } from 'react-native';

// import custom component
import StaticHeader from '../../components/header/StaticHeader';
import Constant from '../../utilities/Constant';
import StoreCard from '../Store/page_components/StoreCard';
import StoreData from './data/store.json';

export default class Store extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: StoreData.data
        };
    }

    handleRedirectUser(page, data) {
        if (page == 'StoreDetail')
            this.props.navigation.navigate(page, { data: data })
        else
            this.props.navigation.navigate(page, { type: "store" })
    }

    render() {
        return (
            <SafeAreaView style={localStyles.container}>
                <StatusBar
                    barStyle="light-content"
                    backgroundColor={Constant.COLOR.PRIMARY}
                />
                <StaticHeader
                    onSearchPressed={() => this.handleRedirectUser('UniversalSearch')}
                />
                <View style={localStyles.contentContainer}>
                    <FlatList
                        data={this.state.data}
                        keyExtractor={item => item.id}
                        contentContainerStyle={{ alignItems: 'center', padding: 8, paddingHorizontal: 16 }}
                        renderItem={({ item, index, separators }) =>
                        (
                            <StoreCard
                                key={index}
                                data={item}
                                onPress={() => this.handleRedirectUser('StoreDetail', item)}
                            />
                        )}
                    />
                </View>
            </SafeAreaView>
        );
    }
}

const localStyles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        flex: 1,
        backgroundColor: Constant.COLOR.PRIMARY,
        justifyContent: 'center',
        alignItems: 'center',
    },
    contentContainer: {
        backgroundColor: Constant.COLOR.ABSOLUTE_WHITE,
        width: Dimensions.get('window').width,
        flex: 1,
    },

})
