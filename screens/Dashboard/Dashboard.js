import React, { Component } from 'react';
import { View, Text, SafeAreaView, StatusBar, Dimensions, StyleSheet, Image } from 'react-native';
import { FlatGrid } from 'react-native-super-grid';

// import custom component
import StaticHeader from '../../components/header/StaticHeader';
import Constant from '../../utilities/Constant';
import ProductCard from '../Products/page_components/ProductCard';
import ProductData from './data/products.json';

export default class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: ProductData.data
        };
    }

    handleRedirectUser(page) {
        this.props.navigation.navigate(page, { type: "product" });
    }

    render() {
        return (
            <SafeAreaView style={localStyles.container}>
                <StatusBar
                    barStyle="light-content"
                    backgroundColor={Constant.COLOR.PRIMARY}
                />
                <StaticHeader
                    onSearchPressed={() => this.handleRedirectUser('UniversalSearch')}
                />
                <View style={{
                    backgroundColor: Constant.COLOR.ABSOLUTE_WHITE,
                    width: Dimensions.get('window').width,
                    flex: 1,
                }}>
                    <FlatGrid
                        itemDimension={130}
                        data={this.state.data}
                        style={localStyles.gridView}
                        showsVerticalScrollIndicator={false}
                        spacing={10}
                        renderItem={({ item }) => (
                            <ProductCard
                                data={item}
                            />
                        )}
                    />
                </View>
            </SafeAreaView>
        );
    }
}

const localStyles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        flex: 1,
        backgroundColor: Constant.COLOR.PRIMARY
    },
    gridView: {
        marginTop: 12,
        flex: 1,
    },

})
