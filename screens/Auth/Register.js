import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, ImageBackground, Platform, Alert, TouchableWithoutFeedback } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { connect } from 'react-redux';

// import custom component
import { currentUser } from '../../redux/actions';
import CustomizeTextInput from '../../components/input/CustomizeTextInput';
import Constant from '../../utilities/Constant';
import CustomizeButton from '../../components/button/CustomizeButton';

class Register extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            email: '',
            password: '',
            confirmationPassword: ''
        };
    }

    handleOnChangeText = (id, value) => {
        if (id == "name") {
            this.setState({ name: value })
        }
        else if (id == "email") {
            this.setState({ email: value })
        }
        else if (id == "password") {
            this.setState({ password: value })
        }
        else {
            this.setState({ confirmationPassword: value })
        }
    }

    handleRedirectUser(page) {
        this.props.navigation.goBack();
    }

    handleSubmitRegister() {
        const { name, email, password, confirmationPassword } = this.state;
        if (name == "") {
            Alert.alert("Warning", "Harap masukan nama anda");
        }
        else if (email == "") {
            Alert.alert("Warning", "Harap masukan email anda");
        }
        else if (password == "") {
            Alert.alert("Warning", "Harap masukan kata sandi anda");
        }
        else if (password != confirmationPassword) {
            Alert.alert("Warning", "Kata sandi dan konfirmasi kata sandi anda harus sama");
        }
        else if (confirmationPassword == "") {
            Alert.alert("Warning", "Harap masukan konfirmasi kata sandi anda");
        }
        else {
            let user = {
                name: name,
                email: email,
                password: password
            }
            this.props.dispatch(currentUser(user));
            this.handleRedirectUser('Login');
        }
    }

    render() {
        return (
            <ImageBackground
                source={{ uri: 'https://images.unsplash.com/photo-1513094735237-8f2714d57c13?ixid=MXwxMjA3fDB8MHxzZWFyY2h8M3x8c2hvcHxlbnwwfDF8MHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60' }}
                style={localStyles.container}
                blurRadius={Platform.OS === 'android' ? 0.5 : 2}
            >
                <LinearGradient
                    style={localStyles.glassContainer}
                    start={[0, 0]}
                    colors={['rgba(255,255,255,0.6)', 'rgba(255,255,255,0.9)']}
                >
                    <Text style={localStyles.title}>Silahkan isi data diri anda</Text>
                    <Text style={localStyles.label}>dan temukan barang-barang terbaik di aplikasi kami</Text>
                    <CustomizeTextInput
                        id="name"
                        isActive={true}
                        placeholder="Name"
                        value={this.state.name}
                        onChangeText={this.handleOnChangeText}
                    />
                    <CustomizeTextInput
                        id="email"
                        isActive={true}
                        placeholder="Email"
                        value={this.state.email}
                        onChangeText={this.handleOnChangeText}
                    />
                    <CustomizeTextInput
                        id="password"
                        isActive={true}
                        placeholder="Password"
                        value={this.state.password}
                        isSecure={true}
                        onChangeText={this.handleOnChangeText}
                    />
                    <CustomizeTextInput
                        id="confirmation-password"
                        isActive={true}
                        placeholder="Confirmation Password"
                        value={this.state.confirmationPassword}
                        isSecure={true}
                        onChangeText={this.handleOnChangeText}
                    />
                    <CustomizeButton
                        text="Register"
                        onPress={() => this.handleSubmitRegister()}
                    />
                    <View style={{ width: "100%", alignItems: 'center', marginTop: 8 }}>
                        <Text style={[localStyles.label, { marginBottom: 0 }]}>didn't have an account? <TouchableWithoutFeedback onPress={() => this.handleRedirectUser('Login')}><Text style={{ fontWeight: 'bold' }}>Register now</Text></TouchableWithoutFeedback></Text>
                    </View>
                </LinearGradient>
            </ImageBackground >
        );
    }
}

const localStyles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'rgba(255,255,255,0.5)',
        justifyContent: 'center',
        alignItems: 'center'
    },
    glassContainer: {
        width: Dimensions.get('window').width * 0.8,
        padding: 24,
        paddingHorizontal: 24,
        backgroundColor: 'rgba(255,255,255,0.5)',
        borderRadius: 16,
    },
    title: {
        color: Constant.COLOR.DARK_LABEL,
        fontWeight: 'bold',
        fontSize: 24,
        marginBottom: 8
    },
    label: {
        color: Constant.COLOR.DARK_LABEL,
        fontSize: 14,
        marginBottom: 16
    },
})

const mapStateToProps = (state) => ({
    user: state.user,
})

//make this component available to the app
export default connect(mapStateToProps)(Register);