export default {
    COLOR: {
        PRIMARY: '#A2C5AC',
        SECONDARY_PRIMARY: '#297373',
        THIN_WHITE: 'rgba(219, 219, 219,0.1)',
        THIN_GRAY: '#c1c1c1',
        DARK_LABEL: "#454545",
        ABSOLUTE_WHITE: '#FFFFFF'
    }
}