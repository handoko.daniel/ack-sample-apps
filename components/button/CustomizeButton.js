import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import Constant from '../../utilities/Constant';

export default class CustomizeButton extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <TouchableOpacity
                onPress={this.props.onPress}
                style={localStyles.container}
            >
                <Text style={localStyles.label}>{this.props.text}</Text>
            </TouchableOpacity>
        );
    }
}

const localStyles = StyleSheet.create({
    container: {
        width: "100%",
        padding: 16,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Constant.COLOR.PRIMARY,
        marginTop: 8,
        borderRadius: 8
    },
    label: {
        color: Constant.COLOR.ABSOLUTE_WHITE,
        fontWeight: 'bold',
        fontSize: 14
    }
})