import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

// import custom component
import Constant from '../../utilities/Constant';

export default class StaticHeader extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    renderHeaderType() {
        const { type } = this.props;
        if (type == "back") {
            return (
                <View style={localStyles.container}>
                    <TouchableOpacity
                        onPress={this.props.onGoBack}
                    >
                        <Icon
                            name="chevron-back"
                            size={24}
                            color={Constant.COLOR.ABSOLUTE_WHITE}
                        />
                    </TouchableOpacity>
                    <Text style={localStyles.headerTitle}>Back</Text>
                </View>
            )
        }
        else {
            return (
                <View style={localStyles.container}>
                    <TouchableOpacity
                        style={localStyles.searchBar}
                        onPress={this.props.onSearchPressed}
                    >
                        <Icon
                            name="ios-search"
                            size={16}
                            color={Constant.COLOR.THIN_GRAY}
                        />
                        <Text style={localStyles.label}>Search...</Text>
                    </TouchableOpacity>
                </View>
            )
        }
    }

    render() {
        return (
            this.renderHeaderType()
        );
    }
}

const localStyles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        backgroundColor: Constant.COLOR.PRIMARY,
        height: 64,
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.18,
        shadowRadius: 1.00,
        elevation: 1,
        alignItems: 'center',
        paddingHorizontal: 16,
        flexDirection: 'row'
    },
    searchBar: {
        backgroundColor: Constant.COLOR.ABSOLUTE_WHITE,
        padding: 12,
        width: "100%",
        borderRadius: 24,
        flexDirection: 'row'
    },
    label: {
        marginLeft: 8,
        color: Constant.COLOR.THIN_GRAY
    },
    headerTitle: {
        marginLeft: 8,
        fontSize: 16,
        color: Constant.COLOR.ABSOLUTE_WHITE
    }
})
