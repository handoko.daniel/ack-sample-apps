import React, { Component } from 'react';
import { Dimensions, StyleSheet, TextInput } from 'react-native';
import Constant from '../../utilities/Constant';

export default class CustomizeTextInput extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    renderInputType() {
        const { isActive } = this.props;
        if (isActive) {
            return (
                <TextInput
                    onChangeText={(value) => this.props.onChangeText(this.props.id, value)}
                    placeholder={this.props.placeholder}
                    value={this.props.value}
                    secureTextEntry={this.props.isSecure}
                    style={localStyles.container}
                />
            )
        }
        else {
            return (
                <TextInput />
            )
        }
    }

    render() {
        return (
            this.renderInputType()
        );
    }
}

const localStyles = StyleSheet.create({
    container: {
        width: "100%",
        borderBottomWidth: 0.5,
        padding: 8,
        paddingHorizontal: 16,
        borderColor: Constant.COLOR.LABEL,
        marginBottom: 8
    }
})