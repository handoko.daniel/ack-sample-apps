export const CURRENT_USER = 'CURRENT_USER';

export function currentUser(payload) {
    return function (dispatch) {
        dispatch({
            type: CURRENT_USER,
            payload
        });
    }
}