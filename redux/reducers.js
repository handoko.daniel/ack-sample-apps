import {
    CURRENT_USER,
} from './actions';

const initialState = {
    user: null,
}

export default (state = initialState, action) => {
    const { type, payload } = action;

    switch (type) {
        case CURRENT_USER:
            return {
                ...state,
                user: payload
            };
        default:
            return state;
    }
};
