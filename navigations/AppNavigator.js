import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Login from '../screens/Auth/Login';
import Register from '../screens/Auth/Register';
import StoreDetail from '../screens/Store/StoreDetail';
import UniversalSearch from '../screens/Search/UniversalSearch';
import ProductDetail from '../screens/Products/ProductDetail';
import Main from './TabNavigator';

const Stack = createStackNavigator();

function App() {
    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen
                    name="Login"
                    component={Login}
                    options={{ headerShown: false }}
                />
                <Stack.Screen
                    name="Register"
                    component={Register}
                    options={{ headerShown: false }}
                />
                <Stack.Screen
                    name="Main"
                    component={Main}
                    options={{ headerShown: false }}
                />
                <Stack.Screen
                    name="StoreDetail"
                    component={StoreDetail}
                    options={{ headerShown: false }}
                />
                <Stack.Screen
                    name="UniversalSearch"
                    component={UniversalSearch}
                    options={{ headerShown: false }}
                />
                <Stack.Screen
                    name="ProductDetail"
                    component={ProductDetail}
                    options={{ headerShown: false }}
                />
            </Stack.Navigator>
        </NavigationContainer>
    );
}

export default App;